﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class respawn : MonoBehaviour {

    [SerializeField] private Transform Ball;
    [SerializeField] private Transform respawnPoint;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        Ball.transform.position = respawnPoint.transform.position;
    }
}
