﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaletasControl : MonoBehaviour {

    public GameObject paleta1;
    public GameObject paleta2;
    public GameObject paleta3;

    public GameObject Ball;
    [SerializeField] private Transform respawnPoint;

    public static bool boost = false;

    public static bool des1 = false;
    public static bool des2 = false;
    public static bool des3 = false;

    public static bool muerte = false;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (des1) {
            paleta1.SetActive(false);
        }
        else 
            paleta1.SetActive(true);
        if (des2) {
            paleta2.SetActive(false);
        }
        else
            paleta2.SetActive(true);
        if (des3) {
            paleta3.SetActive(false);
        }
        else
            paleta3.SetActive(true);

        if (muerte)
        {
            //Ball.SetActive(false);
            Ball.transform.position = respawnPoint.transform.position;
            muerte = false;
        }

        if (des1 == true && des2 == true && des3 == true)
        {
            boost = true;
        }

    }
}
