﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bumper : MonoBehaviour {

    //public GameObject Sphere;
    public int Puntaje = 20;
    public GameObject GameController;

    void OnCollisionEnter(Collision col)
    {
        col.rigidbody.AddForce(-col.contacts[0].normal * 2, ForceMode.Impulse);
        GameControler.Score += Puntaje;
        if (PaletasControl.boost == true){
            GameControler.Score += Puntaje;
        }
    }
}
