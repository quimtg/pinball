﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider col)
    {
        switch (col.gameObject.name)
        {
            case "enable":
                GameControlScript.disabled = false;
                break;
            case "disable":
                GameControlScript.disabled = true;
                break;
            case "paleta1":
                PaletasControl.des1 = true;
                break;
            case "paleta2":
                PaletasControl.des2 = true;
                break;
            case "paleta3":
                PaletasControl.des3 = true;
                break;
            case "RIP":
                PaletasControl.muerte = true;
                break;
        }
    }
}
